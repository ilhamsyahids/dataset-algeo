import os
import shutil

images_path = '.'
train = './train'
test = './test'


folders = [os.path.join(images_path, p)
           for p in sorted(os.listdir(images_path))]
folders = folders[5:]
files = []

for path in folders:
    for image in path:
        files = [os.path.join(path, p)
                 for p in sorted(os.listdir(path))]
        count = 0
        eigth = int(0.8 * len(files)) + 1
        for file in files:
            count += 1
            if count < eigth:
                shutil.copy(file, train)
            else:
                shutil.copy(file, test)
print('DONE.')
